# ACME-Gravel

Forked and edited [Pebble](https://github.com/letsencrypt/pebble)

>A miniature version of [Boulder](https://github.com/letsencrypt/boulder), Pebble
>is a small [ACME-12](https://tools.ietf.org/html/draft-ietf-acme-acme-12) test
> server not suited for use as a production CA.

## !!! WARNING !!!

![WARNING](https://media.giphy.com/media/IT6kBZ1k5oEeI/giphy.gif)

Pebble or Gravel is **NOT INTENDED FOR PRODUCTION OR ANY USE**.

--------

This is edited version of pebble, to be used with custom root certificate.

This version is really hacky, it uses part of [miniCA](https://github.com/jsha/minica)
for loading certs. 

Really the first time i used golang, use on your own!

## Usage 

Everthin is the same as pebble, see [Pebble GH](https://github.com/letsencrypt/pebble)

```
  -ca-cert string
        Root certificate filename, PEM encoded. (default "cert.pem")
  -ca-key string
        Root private key filename, PEM encoded. (default "key.pem")
```

