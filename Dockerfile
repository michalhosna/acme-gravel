FROM golang:alpine as builder

RUN apk add --update --no-cache \
    git

COPY . /go/src/gitlab.com/michalhosna/acme-gravel/

WORKDIR $GOPATH/src/gitlab.com/michalhosna/acme-gravel/

RUN ls -la

RUN go get ./...

RUN go install -i ./...

FROM alpine:3.7
COPY --from=builder /go/bin/pebble /go/bin/pebble

ENTRYPOINT [ "/go/bin/pebble" ]
